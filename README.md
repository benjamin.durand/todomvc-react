# TodoMVC - React

Ce projet est une implémentation du [TodoMVC](https://docs.google.com/document/d/1tj_MuyutV6_vzfDpYeEvTuy4n_9ceKPAX8T5lStg2h0/edit), utilisé avec React.

Le projet a été généré avec [create-react-app](https://create-react-app.dev/docs/getting-started).

## Installation

Exécuter la commande suivante pour installer les dépendances :

`$>npm i`

## Build

Exécuter la commande suivante pour "build" le projet, le dossier de sortie est "./build" :

`$>npm run build`

## Lancement

Exécuter la commande suivante pour lancer l'application :

`$>npm start`

## Tests

Exécuter la commande suivante pour lancer les tests :

`$>npm run test`

## Formatage

Pour formatter le projet, exécuter la commande suivante :

`$>npm run format`
