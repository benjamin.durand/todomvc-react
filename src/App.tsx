import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { FilterType } from './todos/todo.model';
import Todos from './todos/Todos';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Todos selectedFilter={FilterType.ALL} />} />
        <Route
          path="/active"
          element={<Todos selectedFilter={FilterType.ACTIVE} />}
        />
        <Route
          path="/completed"
          element={<Todos selectedFilter={FilterType.COMPLETED} />}
        />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
