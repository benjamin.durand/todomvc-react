import { render, screen } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import userEvent from '@testing-library/user-event';
import React from 'react';
import useOnClickOutside from './useOnClickOutside';

type TestProp = {
  buttonRef: React.RefObject<HTMLButtonElement>;
};

function Test({ buttonRef }: TestProp) {
  return (
    <>
      <input />
      <button ref={buttonRef}>Click on it</button>
    </>
  );
}

describe('useOnClickOutside hook', () => {
  let buttonRef: React.RefObject<HTMLButtonElement>;
  let handlerSpy: jest.Mock;

  beforeEach(() => {
    buttonRef = React.createRef<HTMLButtonElement>();
    handlerSpy = jest.fn();
  });

  test('should return undefined', () => {
    const { result } = renderHook(() =>
      useOnClickOutside(buttonRef, handlerSpy),
    );
    expect(result.all).toEqual([undefined]);
    expect(result.current).toBeUndefined();
    expect(result.error).toBeUndefined();
    expect(handlerSpy).not.toHaveBeenCalled();
  });

  test('should call callback when user clicks outside of the element', async () => {
    render(<Test buttonRef={buttonRef} />);
    renderHook(() => useOnClickOutside(buttonRef, handlerSpy));

    const input = await screen.findByRole('textbox');
    userEvent.click(input);

    expect(handlerSpy).toHaveBeenCalledTimes(1);
    expect(handlerSpy).toHaveBeenCalledWith();
  });

  test('should not call callback when user clicks inside of the element', async () => {
    render(<Test buttonRef={buttonRef} />);
    renderHook(() => useOnClickOutside(buttonRef, handlerSpy));

    const button = await screen.findByRole('button');
    userEvent.click(button);

    expect(handlerSpy).not.toHaveBeenCalled();
  });
});
