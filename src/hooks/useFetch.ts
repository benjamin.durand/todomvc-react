export default function useFetch(api: string) {
  const getMethod = async <T>(id?: string, ac?: AbortController) => {
    const endpoint = id != null ? `${api}/${id}` : api;
    const response = await fetch(endpoint, {
      method: 'GET',
      headers: { 'content-type': 'application/json;charset=UTF-8' },
      signal: ac?.signal,
    });
    if (response.ok) {
      return (await response.json()) as T;
    }
    return Promise.reject(new Error('Fail to get'));
  };

  const postMethod = async <T>(body: any): Promise<T> => {
    const response = await fetch(api, {
      method: 'POST',
      headers: { 'content-type': 'application/json;charset=UTF-8' },
      body: JSON.stringify(body),
    });
    if (response.ok) {
      return (await response.json()) as T;
    }
    return Promise.reject(new Error('Fail to post'));
  };

  const deleteMethod = async (id: string) => {
    const response = await fetch(`${api}/${id}`, {
      method: 'DELETE',
      headers: { 'content-type': 'application/json;charset=UTF-8' },
    });
    if (response.ok) {
      return;
    }
    return Promise.reject(new Error('Fail to delete'));
  };

  const putMethod = async <T>(id: string, body: any) => {
    const response = await fetch(`${api}/${id}`, {
      method: 'PUT',
      headers: { 'content-type': 'application/json;charset=UTF-8' },
      body: JSON.stringify(body),
    });
    if (response.ok) {
      return (await response.json()) as T;
    }
    return Promise.reject(new Error('Fail to put'));
  };

  const patchMethod = async <T>(id: string, body: any) => {
    const response = await fetch(`${api}/${id}`, {
      method: 'PATCH',
      headers: { 'content-type': 'application/json;charset=UTF-8' },
      body: JSON.stringify(body),
    });
    if (response.ok) {
      return (await response.json()) as T;
    }
    return Promise.reject(new Error('Fail to patch'));
  };

  return {
    get: getMethod,
    post: postMethod,
    delete: deleteMethod,
    put: putMethod,
    patch: patchMethod,
  };
}
