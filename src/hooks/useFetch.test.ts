import { renderHook } from '@testing-library/react-hooks';
import useFetch from './useFetch';

describe('useFetch hook', () => {
  test('returns initialized properties', () => {
    const { result } = renderHook(() => useFetch('http://localhost/success'));

    expect(result.current.get).toBeDefined();
    expect(result.current.post).toBeDefined();
    expect(result.current.delete).toBeDefined();
    expect(result.current.put).toBeDefined();
    expect(result.current.patch).toBeDefined();
  });

  describe('calls get', () => {
    test.each`
      parameter    | expectedData
      ${undefined} | ${[]}
      ${'1'}       | ${{}}
    `(
      'and returns expected data',
      async ({
        parameter,
        expectedData,
      }: {
        parameter?: string;
        expectedData: any;
      }) => {
        const { result } = renderHook(() =>
          useFetch('http://localhost/success'),
        );

        const response = await result.current.get(parameter);

        expect(response).toEqual(expectedData);
      },
    );

    test.each`
      parameter
      ${undefined}
      ${'1'}
    `('and aborts signal', async ({ parameter }: { parameter?: string }) => {
      const { result } = renderHook(() => useFetch('http://localhost/success'));

      const ac = new AbortController();
      ac.abort();

      await expect(() =>
        result.current.get(parameter, ac),
      ).rejects.toThrowError('Aborted');
    });

    test.each`
      parameter
      ${undefined}
      ${'1'}
    `('and rejects promise', async ({ parameter }: { parameter?: string }) => {
      const { result } = renderHook(() => useFetch('http://localhost/fail'));

      await expect(() => result.current.get(parameter)).rejects.toThrowError(
        'Fail to get',
      );
    });
  });

  describe('calls post', () => {
    test('and returns expected data', async () => {
      const { result } = renderHook(() => useFetch('http://localhost/success'));

      const response = await result.current.post({});

      expect(response).toEqual({});
    });

    test('and rejects promise', async () => {
      const { result } = renderHook(() => useFetch('http://localhost/fail'));

      await expect(() => result.current.post({})).rejects.toThrowError(
        'Fail to post',
      );
    });
  });

  describe('calls delete', () => {
    test('and returns expected data', async () => {
      const { result } = renderHook(() => useFetch('http://localhost/success'));

      const response = await result.current.delete('1');

      expect(response).toBeUndefined();
    });

    test('and rejects promise', async () => {
      const { result } = renderHook(() => useFetch('http://localhost/fail'));

      await expect(() => result.current.delete('1')).rejects.toThrowError(
        'Fail to delete',
      );
    });
  });

  describe('calls put', () => {
    test('and returns expected data', async () => {
      const { result } = renderHook(() => useFetch('http://localhost/success'));

      const response = await result.current.put('1', {});

      expect(response).toEqual({});
    });

    test('and rejects promise', async () => {
      const { result } = renderHook(() => useFetch('http://localhost/fail'));

      await expect(() => result.current.put('1', {})).rejects.toThrowError(
        'Fail to put',
      );
    });
  });

  describe('calls patch', () => {
    test('and returns expected data', async () => {
      const { result } = renderHook(() => useFetch('http://localhost/success'));

      const response = await result.current.patch('1', {});

      expect(response).toEqual({});
    });

    test('and rejects promise', async () => {
      const { result } = renderHook(() => useFetch('http://localhost/fail'));

      await expect(() => result.current.patch('1', {})).rejects.toThrowError(
        'Fail to patch',
      );
    });
  });
});
