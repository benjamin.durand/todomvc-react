import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import { FilterType } from './todo.model';
import Todos from './Todos';
import useTodos from './useTodos';

jest.mock('./useTodos', () => {
  return jest.fn();
});

describe('Todos component', () => {
  const createTodoSpy = jest.fn();
  const updateTodoSpy = jest.fn();
  const deleteTodoSpy = jest.fn();
  const toggleTodosSpy = jest.fn();
  const clearCompletedTodosSpy = jest.fn();
  const filterItems = [
    { url: '/', name: 'All' },
    { url: '/active', name: 'Active' },
    { url: '/completed', name: 'Completed' },
  ];

  describe('renders correctly', () => {
    test('when there are no todos', () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [],
        displayedTodos: [],
        activeTodos: 0,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: false,
        filterItems: [],
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      const { asFragment } = render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
          </Routes>
        </MemoryRouter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <section
            class="todoapp"
          >
            <header
              class="header"
            >
              <h1>
                todos
              </h1>
              <input
                class="new-todo"
                placeholder="What needs to be done?"
                value=""
              />
            </header>
          </section>
        </DocumentFragment>
      `);

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(deleteTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('when there is one todo', () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [
          {
            id: '1',
            title: 'a title',
            order: 1,
            completed: false,
          },
        ],
        displayedTodos: [
          {
            id: '1',
            title: 'a title',
            order: 1,
            completed: false,
          },
        ],
        activeTodos: 1,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: false,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      const { asFragment } = render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
          </Routes>
        </MemoryRouter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <section
            class="todoapp"
          >
            <header
              class="header"
            >
              <h1>
                todos
              </h1>
              <input
                class="new-todo"
                placeholder="What needs to be done?"
                value=""
              />
            </header>
            <section
              class="main"
            >
              <input
                class="toggle-all"
                id="toggle-all"
                type="checkbox"
              />
              <label
                for="toggle-all"
              >
                Mark all as complete
              </label>
              <ul
                class="todo-list"
              >
                <li
                  class=""
                >
                  <div
                    class="view"
                  >
                    <input
                      class="toggle"
                      type="checkbox"
                    />
                    <label>
                      a title
                    </label>
                    <button
                      class="destroy"
                    />
                  </div>
                </li>
              </ul>
            </section>
            <footer
              class="footer"
            >
              <span
                class="todo-count"
              >
                <strong>
                  1
                </strong>
                 item left
              </span>
              <ul
                class="filters"
              >
                <li>
                  <a
                    aria-current="page"
                    class="selected"
                    href="/"
                  >
                    All
                  </a>
                </li>
                <li>
                  <a
                    class=""
                    href="/active"
                  >
                    Active
                  </a>
                </li>
                <li>
                  <a
                    class=""
                    href="/completed"
                  >
                    Completed
                  </a>
                </li>
              </ul>
            </footer>
          </section>
        </DocumentFragment>
      `);

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(deleteTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('when there are multiple todos', () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [
          { id: '1', title: 'a title', order: 1, completed: false },
          { id: '2', title: 'another title', order: 2, completed: false },
        ],
        displayedTodos: [
          { id: '1', title: 'a title', order: 1, completed: false },
          { id: '2', title: 'another title', order: 2, completed: false },
        ],
        activeTodos: 2,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: false,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      const { asFragment } = render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
          </Routes>
        </MemoryRouter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <section
            class="todoapp"
          >
            <header
              class="header"
            >
              <h1>
                todos
              </h1>
              <input
                class="new-todo"
                placeholder="What needs to be done?"
                value=""
              />
            </header>
            <section
              class="main"
            >
              <input
                class="toggle-all"
                id="toggle-all"
                type="checkbox"
              />
              <label
                for="toggle-all"
              >
                Mark all as complete
              </label>
              <ul
                class="todo-list"
              >
                <li
                  class=""
                >
                  <div
                    class="view"
                  >
                    <input
                      class="toggle"
                      type="checkbox"
                    />
                    <label>
                      a title
                    </label>
                    <button
                      class="destroy"
                    />
                  </div>
                </li>
                <li
                  class=""
                >
                  <div
                    class="view"
                  >
                    <input
                      class="toggle"
                      type="checkbox"
                    />
                    <label>
                      another title
                    </label>
                    <button
                      class="destroy"
                    />
                  </div>
                </li>
              </ul>
            </section>
            <footer
              class="footer"
            >
              <span
                class="todo-count"
              >
                <strong>
                  2
                </strong>
                 items left
              </span>
              <ul
                class="filters"
              >
                <li>
                  <a
                    aria-current="page"
                    class="selected"
                    href="/"
                  >
                    All
                  </a>
                </li>
                <li>
                  <a
                    class=""
                    href="/active"
                  >
                    Active
                  </a>
                </li>
                <li>
                  <a
                    class=""
                    href="/completed"
                  >
                    Completed
                  </a>
                </li>
              </ul>
            </footer>
          </section>
        </DocumentFragment>
      `);

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(deleteTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('when active filter is set', () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [{ id: '1', title: 'a title', order: 1, completed: false }],
        displayedTodos: [
          { id: '1', title: 'a title', order: 1, completed: false },
        ],
        activeTodos: 1,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: false,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      const { asFragment } = render(
        <MemoryRouter initialEntries={['/active']} initialIndex={0}>
          <Routes>
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
          </Routes>
        </MemoryRouter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <section
            class="todoapp"
          >
            <header
              class="header"
            >
              <h1>
                todos
              </h1>
              <input
                class="new-todo"
                placeholder="What needs to be done?"
                value=""
              />
            </header>
            <section
              class="main"
            >
              <input
                class="toggle-all"
                id="toggle-all"
                type="checkbox"
              />
              <label
                for="toggle-all"
              >
                Mark all as complete
              </label>
              <ul
                class="todo-list"
              >
                <li
                  class=""
                >
                  <div
                    class="view"
                  >
                    <input
                      class="toggle"
                      type="checkbox"
                    />
                    <label>
                      a title
                    </label>
                    <button
                      class="destroy"
                    />
                  </div>
                </li>
              </ul>
            </section>
            <footer
              class="footer"
            >
              <span
                class="todo-count"
              >
                <strong>
                  1
                </strong>
                 item left
              </span>
              <ul
                class="filters"
              >
                <li>
                  <a
                    class=""
                    href="/"
                  >
                    All
                  </a>
                </li>
                <li>
                  <a
                    aria-current="page"
                    class="selected"
                    href="/active"
                  >
                    Active
                  </a>
                </li>
                <li>
                  <a
                    class=""
                    href="/completed"
                  >
                    Completed
                  </a>
                </li>
              </ul>
            </footer>
          </section>
        </DocumentFragment>
      `);

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(deleteTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('when completed filter is set', () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [{ id: '1', title: 'a title', order: 1, completed: true }],
        displayedTodos: [
          { id: '1', title: 'a title', order: 1, completed: true },
        ],
        activeTodos: 0,
        isAllDisplayedTodosCompleted: true,
        isSomeTodoCompleted: true,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      const { asFragment } = render(
        <MemoryRouter initialEntries={['/completed']} initialIndex={0}>
          <Routes>
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
          </Routes>
        </MemoryRouter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <section
            class="todoapp"
          >
            <header
              class="header"
            >
              <h1>
                todos
              </h1>
              <input
                class="new-todo"
                placeholder="What needs to be done?"
                value=""
              />
            </header>
            <section
              class="main"
            >
              <input
                checked=""
                class="toggle-all"
                id="toggle-all"
                type="checkbox"
              />
              <label
                for="toggle-all"
              >
                Mark all as complete
              </label>
              <ul
                class="todo-list"
              >
                <li
                  class="completed"
                >
                  <div
                    class="view"
                  >
                    <input
                      checked=""
                      class="toggle"
                      type="checkbox"
                    />
                    <label>
                      a title
                    </label>
                    <button
                      class="destroy"
                    />
                  </div>
                </li>
              </ul>
            </section>
            <footer
              class="footer"
            >
              <span
                class="todo-count"
              >
                <strong>
                  0
                </strong>
                 item left
              </span>
              <ul
                class="filters"
              >
                <li>
                  <a
                    class=""
                    href="/"
                  >
                    All
                  </a>
                </li>
                <li>
                  <a
                    class=""
                    href="/active"
                  >
                    Active
                  </a>
                </li>
                <li>
                  <a
                    aria-current="page"
                    class="selected"
                    href="/completed"
                  >
                    Completed
                  </a>
                </li>
              </ul>
              <button
                class="clear-completed"
              >
                Clear completed
              </button>
            </footer>
          </section>
        </DocumentFragment>
      `);

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(deleteTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });
  });

  describe('calls callback', () => {
    test("createTodo when user types a title and presses 'enter' key", async () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [],
        displayedTodos: [],
        activeTodos: 0,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: false,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
          </Routes>
        </MemoryRouter>,
      );

      const textboxes = await screen.findAllByRole<HTMLInputElement>('textbox');
      const textbox = textboxes[0];
      userEvent.type(textbox, 'a title{enter}');

      expect(createTodoSpy).toHaveBeenCalledTimes(1);
      expect(createTodoSpy).toHaveBeenCalledWith('a title');

      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(deleteTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test.each`
      statusName     | actualStatus | nextStatus
      ${'checked'}   | ${true}      | ${false}
      ${'unchecked'} | ${false}     | ${true}
    `(
      'updateTodo when user clicks on the checkbox button being $statusName',
      async ({
        actualStatus,
        nextStatus,
      }: {
        actualStatus: boolean;
        nextStatus: boolean;
      }) => {
        (useTodos as jest.Mock).mockReturnValue({
          todos: [
            { id: '1', title: 'a title', order: 1, completed: actualStatus },
          ],
          displayedTodos: [
            { id: '1', title: 'a title', order: 1, completed: actualStatus },
          ],
          activeTodos: 1,
          isAllDisplayedTodosCompleted: false,
          isSomeTodoCompleted: false,
          filterItems,
          createTodo: createTodoSpy,
          updateTodo: updateTodoSpy,
          deleteTodo: deleteTodoSpy,
          toggleTodos: toggleTodosSpy,
          clearCompletedTodos: clearCompletedTodosSpy,
        });

        render(
          <MemoryRouter>
            <Routes>
              <Route
                path="/"
                element={<Todos selectedFilter={FilterType.ALL} />}
              />
            </Routes>
          </MemoryRouter>,
        );

        const list = await screen.findAllByRole('list');
        const todoList = list[0];
        const todoListitem = await within(todoList).findByRole('listitem');
        const checkbox = await within(todoListitem).findByRole('checkbox');
        userEvent.click(checkbox);

        expect(updateTodoSpy).toHaveBeenCalledTimes(1);
        expect(updateTodoSpy).toHaveBeenNthCalledWith(1, '1', {
          completed: nextStatus,
        });

        expect(createTodoSpy).not.toHaveBeenCalled();
        expect(deleteTodoSpy).not.toHaveBeenCalled();
        expect(toggleTodosSpy).not.toHaveBeenCalled();
        expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
      },
    );

    test('updateTodo when user enters editing mode, types the new title and presses "enter" key', async () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [{ id: '1', title: 'a title', order: 1, completed: false }],
        displayedTodos: [
          { id: '1', title: 'a title', order: 1, completed: false },
        ],
        activeTodos: 1,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: false,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
          </Routes>
        </MemoryRouter>,
      );

      const list = await screen.findAllByRole('list');
      const todoList = list[0];
      const todoListitem = await within(todoList).findByRole('listitem');
      const label = await within(todoListitem).findByText('a title');
      userEvent.dblClick(label);

      const textbox = await within(todoListitem).findByRole('textbox');
      userEvent.type(textbox, '{space}updated{enter}');

      expect(updateTodoSpy).toHaveBeenCalledTimes(1);
      expect(updateTodoSpy).toHaveBeenCalledWith('1', {
        title: 'a title updated',
      });

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(deleteTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('deleteTodo when user enters editing mode, types a blank title and presses "enter" key', async () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [{ id: '1', title: 'a title', order: 1, completed: false }],
        displayedTodos: [
          { id: '1', title: 'a title', order: 1, completed: false },
        ],
        activeTodos: 1,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: false,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
          </Routes>
        </MemoryRouter>,
      );

      const title = 'a title';

      const list = await screen.findAllByRole('list');
      const todoList = list[0];
      const todoListitem = await within(todoList).findByRole('listitem');
      const label = await within(todoListitem).findByText(title);
      userEvent.dblClick(label);

      const textbox = await within(todoListitem).findByRole<HTMLInputElement>(
        'textbox',
      );
      textbox.setSelectionRange(0, title.length);
      userEvent.type(textbox, '{backspace}{enter}');

      expect(deleteTodoSpy).toHaveBeenCalledTimes(1);
      expect(deleteTodoSpy).toHaveBeenCalledWith('1');

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('deleteTodo when user clicks on the delete button', async () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [{ id: '1', title: 'a title', order: 1, completed: false }],
        displayedTodos: [
          { id: '1', title: 'a title', order: 1, completed: false },
        ],
        activeTodos: 1,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: false,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
          </Routes>
        </MemoryRouter>,
      );

      const buttons = await screen.findAllByRole('button');
      const clearButton = buttons[0];
      userEvent.click(clearButton);

      expect(deleteTodoSpy).toHaveBeenCalledTimes(1);
      expect(deleteTodoSpy).toHaveBeenCalledWith('1');

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('toggleTodo when user clicks on the toggle all button', async () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [{ id: '1', title: 'a title', order: 1, completed: false }],
        displayedTodos: [
          { id: '1', title: 'a title', order: 1, completed: false },
        ],
        activeTodos: 1,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: false,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
          </Routes>
        </MemoryRouter>,
      );

      const checkboxes = await screen.findAllByRole('checkbox');
      const toggleAll = checkboxes[0];
      userEvent.click(toggleAll);

      expect(toggleTodosSpy).toHaveBeenCalledTimes(1);

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(deleteTodoSpy).not.toHaveBeenCalled();
      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('clearCompletedTodos when user clicks on the clear button', async () => {
      (useTodos as jest.Mock).mockReturnValue({
        todos: [{ id: '1', title: 'a title', order: 1, completed: true }],
        displayedTodos: [
          { id: '1', title: 'a title', order: 1, completed: true },
        ],
        activeTodos: 1,
        isAllDisplayedTodosCompleted: false,
        isSomeTodoCompleted: true,
        filterItems,
        createTodo: createTodoSpy,
        updateTodo: updateTodoSpy,
        deleteTodo: deleteTodoSpy,
        toggleTodos: toggleTodosSpy,
        clearCompletedTodos: clearCompletedTodosSpy,
      });

      render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
          </Routes>
        </MemoryRouter>,
      );
      const buttons = await screen.findAllByRole('button');
      const button = buttons[1];
      userEvent.click(button);

      expect(clearCompletedTodosSpy).toHaveBeenCalledTimes(1);

      expect(createTodoSpy).not.toHaveBeenCalled();
      expect(updateTodoSpy).not.toHaveBeenCalled();
      expect(deleteTodoSpy).not.toHaveBeenCalled();
      expect(toggleTodosSpy).not.toHaveBeenCalled();
    });
  });
});
