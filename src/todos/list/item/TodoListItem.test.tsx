import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import TodoListItem from './TodoListItem';

describe('TodoListItem component', () => {
  const onUpdateTodoSpy = jest.fn();
  const onDeleteTodoSpy = jest.fn();

  describe('renders correctly', () => {
    test('when todo is not completed', () => {
      const { asFragment } = render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <li
            class=""
          >
            <div
              class="view"
            >
              <input
                class="toggle"
                type="checkbox"
              />
              <label>
                a title
              </label>
              <button
                class="destroy"
              />
            </div>
          </li>
        </DocumentFragment>
      `);

      expect(onUpdateTodoSpy).not.toHaveBeenCalled();
      expect(onDeleteTodoSpy).not.toHaveBeenCalled();
    });

    test('when todo is completed', () => {
      const { asFragment } = render(
        <TodoListItem
          title={'a title'}
          completed={true}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <li
            class="completed"
          >
            <div
              class="view"
            >
              <input
                checked=""
                class="toggle"
                type="checkbox"
              />
              <label>
                a title
              </label>
              <button
                class="destroy"
              />
            </div>
          </li>
        </DocumentFragment>
      `);

      expect(onUpdateTodoSpy).not.toHaveBeenCalled();
      expect(onDeleteTodoSpy).not.toHaveBeenCalled();
    });
  });

  describe('completed checkbox', () => {
    test('should be unchecked when todo is not completed', async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );
      const li = await screen.findByRole('listitem');
      const checkbox = await within(li).findByRole<HTMLInputElement>(
        'checkbox',
      );
      expect(checkbox).not.toBeChecked();
    });

    test('should be checked when todo is completed', async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={true}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );
      const li = await screen.findByRole('listitem');
      const checkbox = await within(li).findByRole<HTMLInputElement>(
        'checkbox',
      );
      expect(checkbox).toBeChecked();
    });

    test('should call callback onUpdateTodo when toggled', async () => {
      const { rerender } = render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const listitem = await screen.findByRole('listitem');
      const checkbox = await within(listitem).findByRole<HTMLInputElement>(
        'checkbox',
      );

      userEvent.click(checkbox);

      expect(onUpdateTodoSpy).toHaveBeenCalledTimes(1);
      expect(onUpdateTodoSpy).toHaveBeenCalledWith({ completed: true });

      rerender(
        <TodoListItem
          title={'a title'}
          completed={true}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      userEvent.click(checkbox);

      expect(onUpdateTodoSpy).toHaveBeenCalledTimes(2);
      expect(onUpdateTodoSpy).toHaveBeenNthCalledWith(2, { completed: false });
    });
  });

  describe('editing mode', () => {
    test('should be activated when user double clicks on the label', async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const li = await screen.findByRole('listitem');
      const label = await within(li).findByText<HTMLLabelElement>('a title');

      userEvent.dblClick(label);
      expect(li).toHaveClass('editing');
    });

    test('should add autofocus to input', async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const li = await screen.findByRole('listitem');
      const label = await within(li).findByText('a title');

      userEvent.dblClick(label);

      const textbox = await within(li).findByRole('textbox');
      expect(textbox).toHaveFocus();
    });

    test('should persist when user clicks inside of the textbox', async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const li = await screen.findByRole('listitem');
      const label = await within(li).findByText<HTMLLabelElement>('a title');

      userEvent.dblClick(label);
      expect(li).toHaveClass('editing');

      const textbox = await within(li).findByRole<HTMLInputElement>('textbox');
      userEvent.click(textbox);
      expect(li).toHaveClass('editing');
    });

    test('should be deactivated when user clicks outside of the textbox', async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const li = await screen.findByRole('listitem');
      const label = await within(li).findByText<HTMLLabelElement>('a title');

      userEvent.dblClick(label);
      expect(li).toHaveClass('editing');

      userEvent.click(li);
      expect(li).not.toHaveClass('editing');
    });

    test("should be deactivated when user presses 'enter' key", async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const li = await screen.findByRole('listitem');
      const label = await within(li).findByText<HTMLLabelElement>('a title');

      userEvent.dblClick(label);
      expect(li).toHaveClass('editing');

      const textbox = await within(li).findByRole<HTMLInputElement>('textbox');
      userEvent.type(textbox, '{enter}');
      expect(li).not.toHaveClass('editing');
    });

    test("should be deactivated when user presses 'escape' key", async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const li = await screen.findByRole('listitem');
      const label = await within(li).findByText<HTMLLabelElement>('a title');

      userEvent.dblClick(label);
      expect(li).toHaveClass('editing');

      const textbox = await within(li).findByRole<HTMLInputElement>('textbox');
      userEvent.type(textbox, '{esc}');
      expect(li).not.toHaveClass('editing');
    });

    test("should call callback onUpdateTodo when user types not blank title and presses 'enter' key", async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const li = await screen.findByRole('listitem');
      const label = await within(li).findByText<HTMLLabelElement>('a title');

      userEvent.dblClick(label);
      expect(li).toHaveClass('editing');

      const listitem = await screen.findByRole('listitem');
      const textbox = await within(listitem).findByRole<HTMLInputElement>(
        'textbox',
      );

      userEvent.type(textbox, '{enter}');
      expect(onUpdateTodoSpy).toHaveBeenCalledTimes(1);
      expect(onUpdateTodoSpy).toHaveBeenCalledWith({ title: 'a title' });
      expect(onDeleteTodoSpy).not.toHaveBeenCalled();
    });

    test("should call callback onDeleteTodo when user types empty title and presses 'enter' key", async () => {
      const title = 'a title';
      render(
        <TodoListItem
          title={title}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const li = await screen.findByRole('listitem');
      const label = await within(li).findByText<HTMLLabelElement>('a title');

      userEvent.dblClick(label);
      expect(li).toHaveClass('editing');

      const listitem = await screen.findByRole('listitem');
      const textbox = await within(listitem).findByRole<HTMLInputElement>(
        'textbox',
      );

      textbox.setSelectionRange(0, title.length);
      userEvent.type(textbox, '{backspace}{enter}');
      expect(textbox).toHaveValue('');

      expect(onDeleteTodoSpy).toHaveBeenCalledTimes(1);
      expect(onDeleteTodoSpy).toHaveBeenCalledWith();
      expect(onUpdateTodoSpy).not.toHaveBeenCalled();
    });

    test("should call callback onDeleteTodo when user types blank title and presses 'enter' key", async () => {
      const title = 'a title';
      render(
        <TodoListItem
          title={title}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const li = await screen.findByRole('listitem');
      const label = await within(li).findByText<HTMLLabelElement>('a title');

      userEvent.dblClick(label);
      expect(li).toHaveClass('editing');

      const listitem = await screen.findByRole('listitem');
      const textbox = await within(listitem).findByRole<HTMLInputElement>(
        'textbox',
      );

      textbox.setSelectionRange(0, title.length);
      userEvent.type(textbox, '{space}{enter}');
      expect(textbox).toHaveValue(' ');

      expect(onDeleteTodoSpy).toHaveBeenCalledTimes(1);
      expect(onDeleteTodoSpy).toHaveBeenCalledWith();
      expect(onUpdateTodoSpy).not.toHaveBeenCalled();
    });
  });

  describe('delete button', () => {
    test('should call callback onDeleteTodo when clicked', async () => {
      render(
        <TodoListItem
          title={'a title'}
          completed={false}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );

      const listitem = await screen.findByRole('listitem');
      const button = await within(listitem).findByRole('button');

      userEvent.click(button);

      expect(onDeleteTodoSpy).toHaveBeenCalledTimes(1);
      expect(onDeleteTodoSpy).toHaveBeenCalledWith();
      expect(onUpdateTodoSpy).not.toHaveBeenCalled();
    });
  });
});
