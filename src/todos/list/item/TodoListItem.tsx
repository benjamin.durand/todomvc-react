import classNames from 'classnames';
import React, { useCallback, useMemo, useRef, useState } from 'react';
import useOnClickOutside from '../../../hooks/useOnClickOutside';
import { TodoUpdate } from '../../todo.model';

type TodoListItemProps = {
  title: string;
  completed: boolean;
  onUpdateTodo: (todoUpdate: TodoUpdate) => void;
  onDeleteTodo: () => void;
};

export default function TodoListItem({
  title,
  completed,
  onUpdateTodo,
  onDeleteTodo,
}: TodoListItemProps) {
  const [isEditing, setEditing] = useState(false);
  const [newTitle, setNewTitle] = useState(title);

  const inputRef = useRef<HTMLInputElement>(null);
  const onClickOutside = useCallback(() => {
    setNewTitle(title);
    setEditing(false);
  }, [title]);
  useOnClickOutside(inputRef, onClickOutside);

  const todoClass = useMemo(
    () =>
      classNames({
        completed,
        editing: isEditing,
      }),
    [completed, isEditing],
  );

  const onKeyDown = useCallback(
    (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter') {
        newTitle.trim() === ''
          ? onDeleteTodo()
          : onUpdateTodo({ title: newTitle.trim() });
        setNewTitle(newTitle.trim());
        setEditing(false);
      } else if (e.key === 'Escape') {
        setNewTitle(title);
        setEditing(false);
      }
    },
    [newTitle, title, setNewTitle, setEditing, onUpdateTodo, onDeleteTodo],
  );

  return (
    <li className={todoClass}>
      {!isEditing ? (
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={completed}
            onChange={() => onUpdateTodo({ completed: !completed })}
          />
          <label onDoubleClick={() => setEditing(true)}>{title}</label>
          <button className="destroy" onClick={() => onDeleteTodo()}></button>
        </div>
      ) : (
        <input
          ref={inputRef}
          autoFocus
          className="edit"
          value={newTitle}
          onChange={(e) => setNewTitle(e.target.value)}
          onKeyDown={onKeyDown}
        />
      )}
    </li>
  );
}
