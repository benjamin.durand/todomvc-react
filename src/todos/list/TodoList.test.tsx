import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Todo } from '../todo.model';
import TodoList from './TodoList';

describe('TodoList component', () => {
  const onUpdateTodoSpy = jest.fn();
  const onDeleteTodoSpy = jest.fn();

  describe('renders correctly', () => {
    test('when no todos', () => {
      const todos: Todo[] = [];
      const { asFragment } = render(
        <TodoList
          todos={todos}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <ul
            class="todo-list"
          />
        </DocumentFragment>
      `);

      expect(onUpdateTodoSpy).not.toHaveBeenCalled();
      expect(onDeleteTodoSpy).not.toHaveBeenCalled();
    });

    test('when there are todos', () => {
      const todos: Todo[] = [
        { id: '1', title: 'a title', order: 1, completed: false },
        { id: '2', title: 'another title', order: 2, completed: false },
      ];
      const { asFragment } = render(
        <TodoList
          todos={todos}
          onUpdateTodo={onUpdateTodoSpy}
          onDeleteTodo={onDeleteTodoSpy}
        />,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <ul
            class="todo-list"
          >
            <li
              class=""
            >
              <div
                class="view"
              >
                <input
                  class="toggle"
                  type="checkbox"
                />
                <label>
                  a title
                </label>
                <button
                  class="destroy"
                />
              </div>
            </li>
            <li
              class=""
            >
              <div
                class="view"
              >
                <input
                  class="toggle"
                  type="checkbox"
                />
                <label>
                  another title
                </label>
                <button
                  class="destroy"
                />
              </div>
            </li>
          </ul>
        </DocumentFragment>
      `);

      expect(onUpdateTodoSpy).not.toHaveBeenCalled();
      expect(onDeleteTodoSpy).not.toHaveBeenCalled();
    });
  });

  test('calls update callback when user toggles the status of the todo', async () => {
    let todos: Todo[] = [
      { id: '1', title: 'a title', order: 1, completed: false },
    ];
    const { rerender } = render(
      <TodoList
        todos={todos}
        onUpdateTodo={onUpdateTodoSpy}
        onDeleteTodo={onDeleteTodoSpy}
      />,
    );
    const list = await screen.findByRole('list');
    const listItem = await within(list).findByRole('listitem');
    const checkbox = await within(listItem).findByRole('checkbox');

    userEvent.click(checkbox);

    expect(onUpdateTodoSpy).toHaveBeenCalledTimes(1);
    expect(onUpdateTodoSpy).toHaveBeenCalledWith('1', { completed: true });
    expect(onDeleteTodoSpy).not.toHaveBeenCalled();

    todos = [{ id: '1', title: 'a title', order: 1, completed: true }];
    rerender(
      <TodoList
        todos={todos}
        onUpdateTodo={onUpdateTodoSpy}
        onDeleteTodo={onDeleteTodoSpy}
      />,
    );

    userEvent.click(checkbox);

    expect(onUpdateTodoSpy).toHaveBeenCalledTimes(2);
    expect(onUpdateTodoSpy).toHaveBeenNthCalledWith(2, '1', {
      completed: false,
    });
    expect(onDeleteTodoSpy).not.toHaveBeenCalled();
  });

  test('calls update callback when user edits the title of the todo', async () => {
    const todos: Todo[] = [
      { id: '1', title: 'a title', order: 1, completed: false },
    ];
    render(
      <TodoList
        todos={todos}
        onUpdateTodo={onUpdateTodoSpy}
        onDeleteTodo={onDeleteTodoSpy}
      />,
    );
    const list = await screen.findByRole('list');
    const listItem = await within(list).findByRole('listitem');
    const label = await within(listItem).findByText('a title');

    userEvent.dblClick(label);

    const textbox = await within(listItem).findByRole('textbox');
    userEvent.type(textbox, '{space}updated{enter}');

    expect(onUpdateTodoSpy).toHaveBeenCalledTimes(1);
    expect(onUpdateTodoSpy).toHaveBeenCalledWith('1', {
      title: 'a title updated',
    });
    expect(onDeleteTodoSpy).not.toHaveBeenCalled();
  });

  test('calls delete callback when user clicks on the delete button', async () => {
    const todos: Todo[] = [
      { id: '1', title: 'a title', order: 1, completed: false },
    ];
    render(
      <TodoList
        todos={todos}
        onUpdateTodo={onUpdateTodoSpy}
        onDeleteTodo={onDeleteTodoSpy}
      />,
    );
    const list = await screen.findByRole('list');
    const listItem = await within(list).findByRole('listitem');
    const button = await within(listItem).findByRole('button');

    userEvent.click(button);

    expect(onUpdateTodoSpy).not.toHaveBeenCalled();
    expect(onDeleteTodoSpy).toHaveBeenCalledTimes(1);
    expect(onDeleteTodoSpy).toHaveBeenCalledWith('1');
  });
});
