import { Todo, TodoUpdate } from '../todo.model';
import TodoListItem from './item/TodoListItem';

type TodoListProps = {
  todos: Todo[];
  onUpdateTodo: (id: string, todoUpdate: TodoUpdate) => void;
  onDeleteTodo: (id: string) => void;
};

export default function TodoList({
  todos,
  onUpdateTodo,
  onDeleteTodo,
}: TodoListProps) {
  return (
    <ul className="todo-list">
      {todos.map((todo: Todo) => (
        <TodoListItem
          key={todo.id}
          title={todo.title}
          completed={todo.completed}
          onUpdateTodo={(todoUdpate) => onUpdateTodo(todo.id, todoUdpate)}
          onDeleteTodo={() => onDeleteTodo(todo.id)}
        />
      ))}
    </ul>
  );
}
