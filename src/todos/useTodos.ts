import { useCallback, useEffect, useMemo, useState } from 'react';
import useFetch from '../hooks/useFetch';
import { FilterType, Todo, TodoUpdate } from './todo.model';

export default function useTodos(selectedFilter: FilterType) {
  const [todos, setTodos] = useState<Todo[]>([]);
  const [filterType, setFilter] = useState<FilterType>(selectedFilter);

  const {
    get,
    post,
    delete: remove,
    patch,
  } = useFetch(
    process.env.REACT_APP_API != null ? process.env.REACT_APP_API : '',
  );

  useEffect(() => {
    const ac = new AbortController();
    (async () => {
      try {
        const todos = await get<Todo[]>('', ac);
        setTodos(todos);
      } catch (e: unknown) {
        console.error(e);
      }
    })();
    return () => ac.abort();
  }, []);

  useEffect(() => {
    setFilter(selectedFilter);
  }, [selectedFilter]);

  const displayedTodos = useMemo(() => {
    if (FilterType.ACTIVE === filterType) {
      return todos.filter((todo) => !todo.completed);
    } else if (FilterType.COMPLETED === filterType) {
      return todos.filter((todo) => todo.completed);
    } else {
      return todos;
    }
  }, [filterType, todos]);

  const activeTodos = useMemo(
    () => todos.filter((todo) => !todo.completed).length,
    [todos],
  );

  const isAllDisplayedTodosCompleted = useMemo(() => {
    return (
      displayedTodos.length > 0 &&
      displayedTodos.every((todo) => todo.completed)
    );
  }, [displayedTodos]);

  const isSomeTodoCompleted = useMemo(
    () => todos.some((todo) => todo.completed),
    [todos],
  );

  const filterItems = useMemo(
    () => [
      {
        url: '/',
        name: 'All',
      },
      {
        url: '/active',
        name: 'Active',
      },
      {
        url: '/completed',
        name: 'Completed',
      },
    ],
    [],
  );

  const createTodo = useCallback(
    async (title: string) => {
      try {
        const todo = await post<Todo>({
          title,
        });
        setTodos([...todos, todo]);
      } catch (e: unknown) {
        console.error(e);
      }
    },
    [post, todos],
  );

  const updateTodo = useCallback(
    async (id: string, todoUpdate: TodoUpdate) => {
      try {
        const updatedTodo = await patch<Todo>(id, todoUpdate);
        setTodos(
          todos.map((todo) =>
            todo.id === updatedTodo.id ? updatedTodo : todo,
          ),
        );
      } catch (e: unknown) {
        console.error(e);
      }
    },
    [patch, todos],
  );

  const deleteTodo = useCallback(
    async (id: string) => {
      try {
        await remove(id);
        setTodos(todos.filter((todo) => todo.id !== id));
      } catch (e: unknown) {
        console.error(e);
      }
    },
    [remove, todos],
  );

  const toggleTodos = useCallback(async () => {
    const isAllCompleted = todos.every((todo) => todo.completed);

    try {
      const updatedTodos = await Promise.all(
        todos.map((todo) =>
          patch<Todo>(todo.id, { completed: !isAllCompleted }),
        ),
      );
      setTodos(updatedTodos);
    } catch (e: unknown) {
      console.error(e);
    }
  }, [patch, todos]);

  const clearCompletedTodos = useCallback(async () => {
    const completedTodos = todos.filter((todo) => todo.completed);

    try {
      await Promise.all(completedTodos.map((todo) => remove(todo.id)));
      setTodos(
        todos.filter(
          (todo) =>
            !completedTodos.find(
              (completedTodo) => completedTodo.id === todo.id,
            ),
        ),
      );
    } catch (e: unknown) {
      console.error(e);
    }
  }, [remove, todos]);

  return {
    todos,
    displayedTodos,
    activeTodos,
    isAllDisplayedTodosCompleted,
    isSomeTodoCompleted,
    filterItems,
    createTodo,
    updateTodo,
    deleteTodo,
    toggleTodos,
    clearCompletedTodos,
  };
}
