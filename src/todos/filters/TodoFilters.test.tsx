import { render } from '@testing-library/react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import TodoFilters from './TodoFilters';

describe('TodoFilters component', () => {
  describe('renders correctly', () => {
    test('when there are no filter items', () => {
      const { asFragment } = render(
        <MemoryRouter>
          <Routes>
            <Route path="/" element={<TodoFilters filterItems={[]} />} />
          </Routes>
        </MemoryRouter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <ul
            class="filters"
          />
        </DocumentFragment>
      `);
    });

    test('when there is a filter item selected', () => {
      const { asFragment } = render(
        <MemoryRouter>
          <Routes>
            <Route
              path="/"
              element={
                <TodoFilters
                  filterItems={[
                    {
                      url: '/',
                      name: 'All',
                    },
                  ]}
                />
              }
            />
          </Routes>
        </MemoryRouter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <ul
            class="filters"
          >
            <li>
              <a
                aria-current="page"
                class="selected"
                href="/"
              >
                All
              </a>
            </li>
          </ul>
        </DocumentFragment>
      `);
    });

    describe('when on path /', () => {
      test('should have multiple filter items', () => {
        const { asFragment } = render(
          <MemoryRouter>
            <Routes>
              <Route
                path="/"
                element={
                  <TodoFilters
                    filterItems={[
                      {
                        url: '/',
                        name: 'All',
                      },
                      {
                        url: '/active',
                        name: 'Active',
                      },
                      {
                        url: '/completed',
                        name: 'Completed',
                      },
                    ]}
                  />
                }
              />
            </Routes>
          </MemoryRouter>,
        );
        expect(asFragment()).toMatchInlineSnapshot(`
          <DocumentFragment>
            <ul
              class="filters"
            >
              <li>
                <a
                  aria-current="page"
                  class="selected"
                  href="/"
                >
                  All
                </a>
              </li>
              <li>
                <a
                  class=""
                  href="/active"
                >
                  Active
                </a>
              </li>
              <li>
                <a
                  class=""
                  href="/completed"
                >
                  Completed
                </a>
              </li>
            </ul>
          </DocumentFragment>
        `);
      });
    });

    describe('when on path /active', () => {
      test('should have multiple filter items', () => {
        const { asFragment } = render(
          <MemoryRouter initialEntries={['/active']} initialIndex={0}>
            <Routes>
              <Route
                path="/active"
                element={
                  <TodoFilters
                    filterItems={[
                      {
                        url: '/',
                        name: 'All',
                      },
                      {
                        url: '/active',
                        name: 'Active',
                      },
                      {
                        url: '/completed',
                        name: 'Completed',
                      },
                    ]}
                  />
                }
              />
            </Routes>
          </MemoryRouter>,
        );
        expect(asFragment()).toMatchInlineSnapshot(`
          <DocumentFragment>
            <ul
              class="filters"
            >
              <li>
                <a
                  class=""
                  href="/"
                >
                  All
                </a>
              </li>
              <li>
                <a
                  aria-current="page"
                  class="selected"
                  href="/active"
                >
                  Active
                </a>
              </li>
              <li>
                <a
                  class=""
                  href="/completed"
                >
                  Completed
                </a>
              </li>
            </ul>
          </DocumentFragment>
        `);
      });
    });

    describe('when on path /completed', () => {
      test('should have multiple filter items', () => {
        const { asFragment } = render(
          <MemoryRouter initialEntries={['/completed']} initialIndex={0}>
            <Routes>
              <Route
                path="/completed"
                element={
                  <TodoFilters
                    filterItems={[
                      {
                        url: '/',
                        name: 'All',
                      },
                      {
                        url: '/active',
                        name: 'Active',
                      },
                      {
                        url: '/completed',
                        name: 'Completed',
                      },
                    ]}
                  />
                }
              />
            </Routes>
          </MemoryRouter>,
        );
        expect(asFragment()).toMatchInlineSnapshot(`
          <DocumentFragment>
            <ul
              class="filters"
            >
              <li>
                <a
                  class=""
                  href="/"
                >
                  All
                </a>
              </li>
              <li>
                <a
                  class=""
                  href="/active"
                >
                  Active
                </a>
              </li>
              <li>
                <a
                  aria-current="page"
                  class="selected"
                  href="/completed"
                >
                  Completed
                </a>
              </li>
            </ul>
          </DocumentFragment>
        `);
      });
    });
  });
});
