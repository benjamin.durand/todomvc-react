import { NavLink } from 'react-router-dom';
import { FilterItem } from '../todo.model';

type TodoFiltersProps = {
  filterItems: FilterItem[];
};

export default function TodoFilters({ filterItems }: TodoFiltersProps) {
  return (
    <ul className="filters">
      {filterItems.map((filterItem) => (
        <li key={filterItem.name}>
          <NavLink
            to={filterItem.url}
            className={({ isActive }) => (isActive ? 'selected' : '')}
          >
            {filterItem.name}
          </NavLink>
        </li>
      ))}
    </ul>
  );
}
