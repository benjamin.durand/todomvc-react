import { act, renderHook } from '@testing-library/react-hooks';
import useFetch from '../hooks/useFetch';
import { FilterType } from '../todos/todo.model';
import useTodos from './useTodos';

const UUID = '7b520c7b-8cbb-41a1-8479-18e570c67c34';

jest.mock('../hooks/useFetch', () => {
  const mockGet = jest.fn();
  const mockPost = jest.fn();
  const mockDelete = jest.fn();
  const mockPut = jest.fn();
  const mockPatch = jest.fn();
  return () => ({
    get: mockGet,
    post: mockPost,
    delete: mockDelete,
    put: mockPut,
    patch: mockPatch,
  });
});

describe('useTodos hook', () => {
  let mockGet: jest.Mock;
  let mockPost: jest.Mock;
  let mockDelete: jest.Mock;
  let mockPut: jest.Mock;
  let mockPatch: jest.Mock;

  beforeEach(() => {
    const { get, post, delete: remove, patch, put } = useFetch('');

    mockGet = get as jest.Mock;
    mockPost = post as jest.Mock;
    mockDelete = remove as jest.Mock;
    mockPut = put as jest.Mock;
    mockPatch = patch as jest.Mock;

    mockGet.mockResolvedValue([]);
  });

  test('returns initialized object', async () => {
    const { result, waitForNextUpdate } = renderHook(() => {
      return useTodos(FilterType.ALL);
    });

    await waitForNextUpdate();

    expect(result.current.todos).toEqual([]);
    expect(result.current.displayedTodos).toEqual([]);
    expect(result.current.activeTodos).toBe(0);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(false);
    expect(result.current.isSomeTodoCompleted).toBe(false);
    expect(result.current.filterItems).toEqual([
      {
        url: '/',
        name: 'All',
      },
      {
        url: '/active',
        name: 'Active',
      },
      {
        url: '/completed',
        name: 'Completed',
      },
    ]);

    expect(result.current.createTodo).toBeDefined();
    expect(result.current.updateTodo).toBeDefined();
    expect(result.current.deleteTodo).toBeDefined();
    expect(result.current.toggleTodos).toBeDefined();
    expect(result.current.clearCompletedTodos).toBeDefined();

    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith('', new AbortController());

    expect(mockPost).not.toHaveBeenCalled();
    expect(mockDelete).not.toHaveBeenCalled();
    expect(mockPatch).not.toHaveBeenCalled();
    expect(mockPut).not.toHaveBeenCalled();
  });

  test('creates a todo', async () => {
    mockPost.mockResolvedValue({
      id: UUID,
      title: 'a title',
      order: 1,
      completed: false,
    });

    const { result, waitForNextUpdate } = renderHook(() => {
      return useTodos(FilterType.ALL);
    });

    await waitForNextUpdate();

    await act(async () => {
      await result.current.createTodo('a title');
    });

    expect(result.current.todos).toEqual([
      { id: UUID, title: 'a title', order: 1, completed: false },
    ]);
    expect(result.current.displayedTodos).toEqual([
      { id: UUID, title: 'a title', order: 1, completed: false },
    ]);
    expect(result.current.activeTodos).toBe(1);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(false);
    expect(result.current.isSomeTodoCompleted).toBe(false);

    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith('', new AbortController());
    expect(mockPost).toHaveBeenCalledTimes(1);
    expect(mockPost).toHaveBeenCalledWith({ title: 'a title' });

    expect(mockDelete).not.toHaveBeenCalled();
    expect(mockPatch).not.toHaveBeenCalled();
    expect(mockPut).not.toHaveBeenCalled();
  });

  test('deletes a todo', async () => {
    mockGet.mockResolvedValue([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: false,
      },
    ]);
    mockDelete.mockResolvedValue(undefined);

    const { result, waitForNextUpdate } = renderHook(() => {
      return useTodos(FilterType.ALL);
    });

    await waitForNextUpdate();

    await act(async () => {
      await result.current.deleteTodo(UUID);
    });

    expect(result.current.todos).toEqual([]);
    expect(result.current.displayedTodos).toEqual([]);
    expect(result.current.activeTodos).toBe(0);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(false);
    expect(result.current.isSomeTodoCompleted).toBe(false);

    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith('', new AbortController());
    expect(mockDelete).toHaveBeenCalledTimes(1);
    expect(mockDelete).toHaveBeenCalledWith(UUID);

    expect(mockPost).not.toHaveBeenCalled();
    expect(mockPatch).not.toHaveBeenCalled();
    expect(mockPut).not.toHaveBeenCalled();
  });

  test('updates a todo', async () => {
    mockGet.mockResolvedValue([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: false,
      },
    ]);
    mockPatch.mockResolvedValue({
      id: UUID,
      title: 'updated title',
      order: 1,
      completed: true,
    });

    const { result, waitForNextUpdate } = renderHook(() => {
      return useTodos(FilterType.ALL);
    });

    await waitForNextUpdate();

    await act(async () => {
      await result.current.updateTodo(UUID, {
        title: 'updated title',
        completed: true,
      });
    });

    expect(result.current.todos).toEqual([
      { id: UUID, title: 'updated title', order: 1, completed: true },
    ]);
    expect(result.current.displayedTodos).toEqual([
      { id: UUID, title: 'updated title', order: 1, completed: true },
    ]);
    expect(result.current.activeTodos).toBe(0);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(true);
    expect(result.current.isSomeTodoCompleted).toBe(true);

    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith('', new AbortController());
    expect(mockPatch).toHaveBeenCalledTimes(1);
    expect(mockPatch).toHaveBeenCalledWith(UUID, {
      title: 'updated title',
      completed: true,
    });

    expect(mockPost).not.toHaveBeenCalled();
    expect(mockDelete).not.toHaveBeenCalled();
    expect(mockPut).not.toHaveBeenCalled();
  });

  test('toggles todos', async () => {
    const OTHER_UUID = '2104b1dc-6059-4c0d-8734-3f226b4c1c67';
    mockGet.mockResolvedValue([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: false,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);
    mockPatch.mockResolvedValueOnce({
      id: UUID,
      title: 'a title',
      order: 1,
      completed: true,
    });
    mockPatch.mockResolvedValueOnce({
      id: OTHER_UUID,
      title: 'another title',
      order: 2,
      completed: true,
    });

    const { result, waitForNextUpdate } = renderHook(() => {
      return useTodos(FilterType.ALL);
    });

    await waitForNextUpdate();

    await act(async () => {
      await result.current.toggleTodos();
    });

    expect(result.current.todos).toEqual([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: true,
      },
    ]);
    expect(result.current.displayedTodos).toEqual([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: true,
      },
    ]);
    expect(result.current.activeTodos).toBe(0);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(true);
    expect(result.current.isSomeTodoCompleted).toBe(true);

    mockPatch.mockResolvedValueOnce({
      id: UUID,
      title: 'a title',
      order: 1,
      completed: false,
    });
    mockPatch.mockResolvedValueOnce({
      id: OTHER_UUID,
      title: 'another title',
      order: 2,
      completed: false,
    });

    await act(async () => {
      await result.current.toggleTodos();
    });

    expect(result.current.todos).toEqual([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: false,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);
    expect(result.current.displayedTodos).toEqual([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: false,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);
    expect(result.current.activeTodos).toBe(2);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(false);
    expect(result.current.isSomeTodoCompleted).toBe(false);

    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith('', new AbortController());
    expect(mockPatch).toHaveBeenCalledTimes(4);
    expect(mockPatch).toHaveBeenNthCalledWith(1, UUID, {
      completed: true,
    });
    expect(mockPatch).toHaveBeenNthCalledWith(2, OTHER_UUID, {
      completed: true,
    });
    expect(mockPatch).toHaveBeenNthCalledWith(3, UUID, {
      completed: false,
    });
    expect(mockPatch).toHaveBeenNthCalledWith(4, OTHER_UUID, {
      completed: false,
    });

    expect(mockPost).not.toHaveBeenCalled();
    expect(mockDelete).not.toHaveBeenCalled();
    expect(mockPut).not.toHaveBeenCalled();
  });

  test('clears completed todos', async () => {
    const OTHER_UUID = '2104b1dc-6059-4c0d-8734-3f226b4c1c67';
    mockGet.mockResolvedValue([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: true,
      },
    ]);

    const { result, waitForNextUpdate } = renderHook(() => {
      return useTodos(FilterType.ALL);
    });

    await waitForNextUpdate();

    await act(async () => {
      await result.current.clearCompletedTodos();
    });

    expect(result.current.todos).toEqual([]);
    expect(result.current.displayedTodos).toEqual([]);
    expect(result.current.activeTodos).toBe(0);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(false);
    expect(result.current.isSomeTodoCompleted).toBe(false);

    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith('', new AbortController());
    expect(mockDelete).toHaveBeenCalledTimes(2);
    expect(mockDelete).toHaveBeenNthCalledWith(1, UUID);
    expect(mockDelete).toHaveBeenNthCalledWith(2, OTHER_UUID);

    expect(mockPost).not.toHaveBeenCalled();
    expect(mockPatch).not.toHaveBeenCalled();
    expect(mockPut).not.toHaveBeenCalled();
  });

  test('gets displayed todos when all filter is selected', async () => {
    const OTHER_UUID = '2104b1dc-6059-4c0d-8734-3f226b4c1c67';
    mockGet.mockResolvedValue([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);

    const { result, waitForNextUpdate } = renderHook(() => {
      return useTodos(FilterType.ALL);
    });

    await waitForNextUpdate();

    expect(result.current.todos).toEqual([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);
    expect(result.current.displayedTodos).toEqual([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);
    expect(result.current.activeTodos).toBe(1);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(false);
    expect(result.current.isSomeTodoCompleted).toBe(true);

    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith('', new AbortController());

    expect(mockPost).not.toHaveBeenCalled();
    expect(mockDelete).not.toHaveBeenCalled();
    expect(mockPatch).not.toHaveBeenCalled();
    expect(mockPut).not.toHaveBeenCalled();
  });

  test('gets displayed todos when active filter is selected', async () => {
    const OTHER_UUID = '2104b1dc-6059-4c0d-8734-3f226b4c1c67';
    mockGet.mockResolvedValue([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);

    const { result, waitForNextUpdate } = renderHook(() => {
      return useTodos(FilterType.ACTIVE);
    });

    await waitForNextUpdate();

    expect(result.current.todos).toEqual([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);
    expect(result.current.displayedTodos).toEqual([
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);
    expect(result.current.activeTodos).toBe(1);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(false);
    expect(result.current.isSomeTodoCompleted).toBe(true);

    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith('', new AbortController());

    expect(mockPost).not.toHaveBeenCalled();
    expect(mockDelete).not.toHaveBeenCalled();
    expect(mockPatch).not.toHaveBeenCalled();
    expect(mockPut).not.toHaveBeenCalled();
  });

  test('gets displayed todos when completed filter is selected', async () => {
    const OTHER_UUID = '2104b1dc-6059-4c0d-8734-3f226b4c1c67';
    mockGet.mockResolvedValue([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);

    const { result, waitForNextUpdate } = renderHook(() => {
      return useTodos(FilterType.COMPLETED);
    });

    await waitForNextUpdate();

    expect(result.current.todos).toEqual([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
      {
        id: OTHER_UUID,
        title: 'another title',
        order: 2,
        completed: false,
      },
    ]);
    expect(result.current.displayedTodos).toEqual([
      {
        id: UUID,
        title: 'a title',
        order: 1,
        completed: true,
      },
    ]);
    expect(result.current.activeTodos).toBe(1);
    expect(result.current.isAllDisplayedTodosCompleted).toBe(true);
    expect(result.current.isSomeTodoCompleted).toBe(true);

    expect(mockGet).toHaveBeenCalledTimes(1);
    expect(mockGet).toHaveBeenCalledWith('', new AbortController());

    expect(mockPost).not.toHaveBeenCalled();
    expect(mockDelete).not.toHaveBeenCalled();
    expect(mockPatch).not.toHaveBeenCalled();
    expect(mockPut).not.toHaveBeenCalled();
  });
});
