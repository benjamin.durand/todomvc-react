import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import TodoToggleAll from './TodoToggleAll';

describe('TodoToggleAll component', () => {
  const onToggleTodoSpy = jest.fn();

  describe('renders correctly', () => {
    test('when todos are completed', () => {
      const { asFragment } = render(
        <TodoToggleAll
          isAllTodosCompleted={true}
          onToggleTodo={onToggleTodoSpy}
        />,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <input
            checked=""
            class="toggle-all"
            id="toggle-all"
            type="checkbox"
          />
          <label
            for="toggle-all"
          >
            Mark all as complete
          </label>
        </DocumentFragment>
      `);

      expect(onToggleTodoSpy).not.toHaveBeenCalled();
    });

    test('when a todo is not completed', () => {
      const { asFragment } = render(
        <TodoToggleAll
          isAllTodosCompleted={false}
          onToggleTodo={onToggleTodoSpy}
        />,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <input
            class="toggle-all"
            id="toggle-all"
            type="checkbox"
          />
          <label
            for="toggle-all"
          >
            Mark all as complete
          </label>
        </DocumentFragment>
      `);

      expect(onToggleTodoSpy).not.toHaveBeenCalled();
    });
  });

  test.each`
    isChecked
    ${true}
    ${false}
  `(
    'calls callback when user clicks on the checkbox no matter the value',
    async ({ isChecked }: { isChecked: boolean }) => {
      render(
        <TodoToggleAll
          isAllTodosCompleted={isChecked}
          onToggleTodo={onToggleTodoSpy}
        />,
      );
      const checkbox = await screen.findByRole<HTMLInputElement>('checkbox');
      expect(checkbox.checked).toBe(isChecked);

      userEvent.click(checkbox);

      expect(onToggleTodoSpy).toHaveBeenCalledTimes(1);
    },
  );
});
