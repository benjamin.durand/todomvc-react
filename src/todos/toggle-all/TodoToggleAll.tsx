type TodoToggleAllProps = {
  isAllTodosCompleted: boolean;
  onToggleTodo: () => void;
};

export default function TodoToggleAll({
  isAllTodosCompleted,
  onToggleTodo,
}: TodoToggleAllProps) {
  return (
    <>
      <input
        id="toggle-all"
        className="toggle-all"
        type="checkbox"
        onChange={onToggleTodo}
        checked={isAllTodosCompleted}
      />
      <label htmlFor="toggle-all">Mark all as complete</label>
    </>
  );
}
