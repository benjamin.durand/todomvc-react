import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import TodoCreate from './TodoCreate';

describe('TodoCreate component', () => {
  const onCreateTodoSpy = jest.fn();

  test('renders correctly', async () => {
    const { asFragment } = render(
      <TodoCreate onCreateTodo={onCreateTodoSpy} />,
    );
    expect(asFragment()).toMatchInlineSnapshot(`
      <DocumentFragment>
        <input
          class="new-todo"
          placeholder="What needs to be done?"
          value=""
        />
      </DocumentFragment>
    `);

    const input = await screen.findByRole('textbox');
    expect(input).toHaveFocus();
    expect(onCreateTodoSpy).not.toHaveBeenCalled();
  });

  test("calls callback when user presses 'enter' key", async () => {
    render(<TodoCreate onCreateTodo={onCreateTodoSpy} />);
    const textbox = await screen.findByRole('textbox');

    userEvent.type(textbox, 'a title');
    expect(textbox).toHaveValue('a title');
    expect(onCreateTodoSpy).not.toHaveBeenCalled();

    userEvent.type(textbox, '{enter}');
    expect(textbox).toHaveValue('');
    expect(onCreateTodoSpy).toHaveBeenCalledTimes(1);
    expect(onCreateTodoSpy).toHaveBeenCalledWith('a title');
  });

  test("doesn't call callback when user presses 'enter' key with empty title", async () => {
    render(<TodoCreate onCreateTodo={onCreateTodoSpy} />);
    const textbox = await screen.findByRole('textbox');

    expect(textbox).toHaveValue('');
    expect(onCreateTodoSpy).not.toHaveBeenCalled();

    userEvent.type(textbox, '{enter}');
    expect(textbox).toHaveValue('');
    expect(onCreateTodoSpy).not.toHaveBeenCalled();
  });

  test("doesn't call callback when user presses 'enter' key with blank title", async () => {
    render(<TodoCreate onCreateTodo={onCreateTodoSpy} />);
    const textbox = await screen.findByRole('textbox');

    userEvent.type(textbox, '{space}');
    expect(textbox).toHaveValue(' ');
    expect(onCreateTodoSpy).not.toHaveBeenCalled();

    userEvent.type(textbox, '{enter}');
    expect(textbox).toHaveValue(' ');
    expect(onCreateTodoSpy).not.toHaveBeenCalled();
  });
});
