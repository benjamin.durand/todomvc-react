import { useCallback, useState } from 'react';

type TodoCreateProps = {
  onCreateTodo: (title: string) => void;
};

export default function TodoCreate({ onCreateTodo }: TodoCreateProps) {
  const [title, setTitle] = useState('');
  const onKeyDown = useCallback(
    (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter' && title.trim() !== '') {
        onCreateTodo(title.trim());
        setTitle('');
      }
    },
    [onCreateTodo, setTitle, title],
  );
  return (
    <input
      className="new-todo"
      placeholder="What needs to be done?"
      value={title}
      onChange={(e) => setTitle(e.target.value)}
      onKeyDown={onKeyDown}
      autoFocus
    />
  );
}
