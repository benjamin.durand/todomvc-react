type TodoFooterProps = React.PropsWithChildren<{
  todosLeft: number;
  showClearButton: boolean;
  onClearTodo: () => void;
}>;

export default function TodoFooter({
  todosLeft,
  showClearButton,
  onClearTodo,
  children,
}: TodoFooterProps) {
  return (
    <footer className="footer">
      <span className="todo-count">
        <strong>{todosLeft}</strong> {todosLeft > 1 ? 'items' : 'item'} left
      </span>
      {children}
      {showClearButton && (
        <button className="clear-completed" onClick={onClearTodo}>
          Clear completed
        </button>
      )}
    </footer>
  );
}
