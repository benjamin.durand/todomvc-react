import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import TodoFooter from './TodoFooter';

describe('TodoFooter component', () => {
  const clearCompletedTodosSpy = jest.fn();

  describe('renders correctly', () => {
    test('when the clear button is displayed', () => {
      const { asFragment } = render(
        <TodoFooter
          todosLeft={0}
          showClearButton={true}
          onClearTodo={clearCompletedTodosSpy}
        >
          <div>a child</div>
        </TodoFooter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <footer
            class="footer"
          >
            <span
              class="todo-count"
            >
              <strong>
                0
              </strong>
               item left
            </span>
            <div>
              a child
            </div>
            <button
              class="clear-completed"
            >
              Clear completed
            </button>
          </footer>
        </DocumentFragment>
      `);

      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('when the clear button is not displayed', () => {
      const { asFragment } = render(
        <TodoFooter
          todosLeft={0}
          showClearButton={false}
          onClearTodo={clearCompletedTodosSpy}
        >
          <div>a child</div>
        </TodoFooter>,
      );
      expect(asFragment).toMatchInlineSnapshot(`[Function]`);

      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('when there is one todo left', () => {
      const { asFragment } = render(
        <TodoFooter
          todosLeft={1}
          showClearButton={false}
          onClearTodo={clearCompletedTodosSpy}
        >
          <div>a child</div>
        </TodoFooter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <footer
            class="footer"
          >
            <span
              class="todo-count"
            >
              <strong>
                1
              </strong>
               item left
            </span>
            <div>
              a child
            </div>
          </footer>
        </DocumentFragment>
      `);

      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });

    test('when there are at least two todos', () => {
      const { asFragment } = render(
        <TodoFooter
          todosLeft={2}
          showClearButton={false}
          onClearTodo={clearCompletedTodosSpy}
        >
          <div>a child</div>
        </TodoFooter>,
      );
      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <footer
            class="footer"
          >
            <span
              class="todo-count"
            >
              <strong>
                2
              </strong>
               items left
            </span>
            <div>
              a child
            </div>
          </footer>
        </DocumentFragment>
      `);

      expect(clearCompletedTodosSpy).not.toHaveBeenCalled();
    });
  });

  describe('clear button', () => {
    test('should call callback when clicked', async () => {
      render(
        <TodoFooter
          todosLeft={0}
          showClearButton={true}
          onClearTodo={clearCompletedTodosSpy}
        >
          <div>a child</div>
        </TodoFooter>,
      );

      const button = await screen.findByRole('button');

      userEvent.click(button);

      expect(clearCompletedTodosSpy).toHaveBeenCalledTimes(1);
    });
  });
});
