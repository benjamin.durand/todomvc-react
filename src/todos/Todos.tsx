import TodoCreate from './create/TodoCreate';
import TodoFilters from './filters/TodoFilters';
import TodoFooter from './footer/TodoFooter';
import TodoList from './list/TodoList';
import { FilterType } from './todo.model';
import TodoToggleAll from './toggle-all/TodoToggleAll';
import useTodos from './useTodos';

type TodosProps = {
  selectedFilter: FilterType;
};

export default function Todos({ selectedFilter }: TodosProps) {
  const {
    todos,
    displayedTodos,
    activeTodos,
    isAllDisplayedTodosCompleted,
    isSomeTodoCompleted,
    filterItems,
    createTodo,
    updateTodo,
    deleteTodo,
    toggleTodos,
    clearCompletedTodos,
  } = useTodos(selectedFilter);

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <TodoCreate onCreateTodo={createTodo} />
      </header>
      {todos.length > 0 && (
        <>
          <section className="main">
            <TodoToggleAll
              isAllTodosCompleted={isAllDisplayedTodosCompleted}
              onToggleTodo={toggleTodos}
            />
            <TodoList
              todos={displayedTodos}
              onUpdateTodo={updateTodo}
              onDeleteTodo={deleteTodo}
            />
          </section>
          <TodoFooter
            todosLeft={activeTodos}
            showClearButton={isSomeTodoCompleted}
            onClearTodo={clearCompletedTodos}
          >
            <TodoFilters filterItems={filterItems} />
          </TodoFooter>
        </>
      )}
    </section>
  );
}
