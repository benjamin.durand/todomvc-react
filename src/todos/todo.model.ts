export enum FilterType {
  ALL,
  ACTIVE,
  COMPLETED,
}

export type FilterItem = {
  url: string;
  name: string;
};

export type Todo = {
  id: string;
  title: string;
  order: number;
  completed: boolean;
};

export type TodoUpdate = Partial<Pick<Todo, 'title' | 'completed'>>;
