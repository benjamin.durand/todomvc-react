import { rest } from 'msw';

const api = process.env.REACT_APP_API != null ? process.env.REACT_APP_API : '';

export const handlers = [
  // success
  rest.get('/success', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json([]));
  }),

  rest.get('/success/:todoId', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({}));
  }),

  rest.post('/success', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({}));
  }),

  rest.delete('/success/:todoId', (req, res, ctx) => {
    return res(ctx.status(200));
  }),

  rest.put('/success/:todoId', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({}));
  }),

  rest.patch('/success/:todoId', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({}));
  }),

  // fail
  rest.get('/fail', (req, res, ctx) => {
    return res(ctx.status(400));
  }),

  rest.get('/fail/:id', (req, res, ctx) => {
    return res(ctx.status(400));
  }),

  rest.post('/fail', (req, res, ctx) => {
    return res(ctx.status(400));
  }),

  rest.delete('/fail/:id', (req, res, ctx) => {
    return res(ctx.status(400));
  }),

  rest.put('/fail/:id', (req, res, ctx) => {
    return res(ctx.status(400));
  }),

  rest.patch('/fail/:id', (req, res, ctx) => {
    return res(ctx.status(400));
  }),

  rest.patch('/fail/:id', (req, res, ctx) => {
    return res(ctx.status(400));
  }),

  // todos
  rest.get(api, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json([]));
  }),

  rest.post(api, (req, res, ctx) => {
    return res(
      ctx.status(201),
      ctx.json({
        id: '6573f449-3427-49b7-8f76-7838d5d738b6',
        title: (req.body as any).title,
        order: 1,
        completed: false,
      }),
    );
  }),

  rest.patch(`${api}/:todoId`, (req, res, ctx) => {
    const { todoId } = req.params;
    return res(
      ctx.status(200),
      ctx.json({
        id: todoId,
        title: (req.body as any).title,
        order: 1,
        completed: false,
      }),
    );
  }),

  rest.delete(`${api}/:todoId`, (req, res, ctx) => {
    return res(ctx.status(200));
  }),
];
