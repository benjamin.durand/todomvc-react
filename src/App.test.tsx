import {
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { rest, server } from './mocks/server';
import { FilterType } from './todos/todo.model';
import Todos from './todos/Todos';

describe('Integration tests', () => {
  const api =
    process.env.REACT_APP_API != null ? process.env.REACT_APP_API : '';

  describe('initialize app', () => {
    test('should renders correctly', async () => {
      jest.spyOn(console, 'error');
      const { asFragment } = render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      expect(asFragment()).toMatchInlineSnapshot(`
        <DocumentFragment>
          <section
            class="todoapp"
          >
            <header
              class="header"
            >
              <h1>
                todos
              </h1>
              <input
                class="new-todo"
                placeholder="What needs to be done?"
                value=""
              />
            </header>
          </section>
        </DocumentFragment>
      `);
    });
  });

  describe('create todo', () => {
    test('should find the title of the created todo', async () => {
      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      await expect(() => screen.findAllByRole('list')).rejects.toThrow();

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      expect(lists).toHaveLength(2);

      const todosList = lists[0];
      const todoListitem = await within(todosList).findByRole('listitem');
      const todoTitle = await within(todoListitem).findByText('a title');
      const checkbox = await within(todoListitem).findByRole('checkbox');

      expect(todoTitle).toBeInTheDocument();
      expect(checkbox).not.toBeChecked();
    });
  });

  describe('update todo', () => {
    test('should find the updated title of the todo', async () => {
      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      const todosList = lists[0];
      const todoListitem = await within(todosList).findByRole('listitem');
      const label = await within(todoListitem).findByText('a title');

      userEvent.dblClick(label);

      const input = await within(todoListitem).findByRole('textbox');
      userEvent.type(input, '{space}updated{enter}');

      const todoTitle = await within(todoListitem).findByText(
        'a title updated',
      );
      const checkbox = await within(todoListitem).findByRole('checkbox');

      expect(todoTitle).toBeInTheDocument();
      expect(checkbox).not.toBeChecked();
    });

    test('should find the updated status of the todo', async () => {
      server.use(
        rest.patch(`${api}/:todoId`, (req, res, ctx) => {
          const { todoId } = req.params;
          return res(
            ctx.status(200),
            ctx.json({
              id: todoId,
              title: 'a title',
              order: 1,
              completed: (req.body as any).completed,
            }),
          );
        }),
      );

      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      const todosList = lists[0];
      const todoListitem = await within(todosList).findByRole('listitem');
      const checkbox = await within(todoListitem).findByRole<HTMLInputElement>(
        'checkbox',
      );

      expect(checkbox).not.toBeChecked();

      userEvent.click(checkbox);

      await waitFor(() => {
        expect(checkbox).toBeChecked();
      });
    });
  });

  describe('delete todo', () => {
    test('should not find the removed todo', async () => {
      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      const todosList = lists[0];
      const todoListitem = await within(todosList).findByRole('listitem');
      const button = await within(todoListitem).findByRole('button');

      userEvent.click(button);

      await waitForElementToBeRemoved(todosList);
      await expect(() => screen.findAllByRole('list')).rejects.toThrow();
    });
  });

  describe('toggle all todos status', () => {
    test('should find the updated status of the todo', async () => {
      server.use(
        rest.patch(`${api}/:todoId`, (req, res, ctx) => {
          const { todoId } = req.params;
          return res(
            ctx.status(200),
            ctx.json({
              id: todoId,
              title: 'a title',
              order: 1,
              completed: (req.body as any).completed,
            }),
          );
        }),
      );

      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      const todosList = lists[0];
      const checkbox = await within(todosList).findByRole('checkbox');
      expect(checkbox).not.toBeChecked();

      const checkboxes = await screen.findAllByRole('checkbox');
      expect(checkboxes).toHaveLength(2);
      const toggleAll = checkboxes[0];

      userEvent.click(toggleAll);

      await waitFor(() => {
        expect(checkbox).toBeChecked();
      });
    });
  });

  describe('clear completed todos', () => {
    test('should not find removed todo', async () => {
      server.use(
        rest.patch(`${api}/:todoId`, (req, res, ctx) => {
          const { todoId } = req.params;
          return res(
            ctx.status(200),
            ctx.json({
              id: todoId,
              title: 'a title',
              order: 1,
              completed: (req.body as any).completed,
            }),
          );
        }),
      );

      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      const todosList = lists[0];
      const checkbox = await within(todosList).findByRole('checkbox');
      expect(checkbox).not.toBeChecked();

      userEvent.click(checkbox);

      await waitFor(() => expect(checkbox).toBeChecked());

      const buttons = await screen.findAllByRole('button');
      expect(buttons).toHaveLength(2);
      const clearCompletedButton = buttons[1];

      userEvent.click(clearCompletedButton);

      await waitFor(() => {
        expect(todosList).not.toBeInTheDocument();
      });
    });
  });

  describe('set active filter', () => {
    test('should find the active todo', async () => {
      server.use(
        rest.patch(`${api}/:todoId`, (req, res, ctx) => {
          const { todoId } = req.params;
          return res(
            ctx.status(200),
            ctx.json({
              id: todoId,
              title: 'a title',
              order: 1,
              completed: (req.body as any).completed,
            }),
          );
        }),
      );

      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      const [todosList, filtersList] = lists;
      const filtersListItem = await within(filtersList).findAllByRole(
        'listitem',
      );
      const activeFilter = filtersListItem[1];
      const link = await within(activeFilter).findByRole('link');

      userEvent.click(link);

      await expect(async () => {
        return await waitForElementToBeRemoved(() =>
          within(todosList).findAllByRole('listitem'),
        );
      }).rejects.toThrow();

      const allFilter = filtersListItem[0];
      const allLink = await within(allFilter).findByRole('link');
      userEvent.click(allLink);
    });

    test('should not find the completed todo', async () => {
      server.use(
        rest.patch(`${api}/:todoId`, (req, res, ctx) => {
          const { todoId } = req.params;
          return res(
            ctx.status(200),
            ctx.json({
              id: todoId,
              title: 'a title',
              order: 1,
              completed: (req.body as any).completed,
            }),
          );
        }),
      );

      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      const [todosList, filtersList] = lists;
      const todosListitem = await within(todosList).findAllByRole('listitem');
      expect(todosListitem).toHaveLength(1);
      const checkbox = await within(todosListitem[0]).findByRole('checkbox');

      userEvent.click(checkbox);

      await waitFor(() => expect(checkbox).toBeChecked());

      const filtersListItem = await within(filtersList).findAllByRole(
        'listitem',
      );
      const activeFilter = filtersListItem[1];
      const link = await within(activeFilter).findByRole('link');

      userEvent.click(link);

      await expect(() =>
        within(todosList).findAllByRole('listitem'),
      ).rejects.toThrow();

      const allFilter = filtersListItem[0];
      const allLink = await within(allFilter).findByRole('link');
      userEvent.click(allLink);
    });
  });

  describe('set completed filter', () => {
    test('should find the completed todo', async () => {
      server.use(
        rest.patch(`${api}/:todoId`, (req, res, ctx) => {
          const { todoId } = req.params;
          return res(
            ctx.status(200),
            ctx.json({
              id: todoId,
              title: 'a title',
              order: 1,
              completed: (req.body as any).completed,
            }),
          );
        }),
      );

      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      const [todosList, filtersList] = lists;
      const todosListitem = await within(todosList).findAllByRole('listitem');
      const checkbox = await within(todosListitem[0]).findByRole('checkbox');

      userEvent.click(checkbox);

      await waitFor(() => expect(checkbox).toBeChecked());

      const filtersListItem = await within(filtersList).findAllByRole(
        'listitem',
      );
      const completedFilter = filtersListItem[2];
      const link = await within(completedFilter).findByRole('link');

      userEvent.click(link);

      await expect(async () => {
        return await waitForElementToBeRemoved(() =>
          within(todosList).findAllByRole('listitem'),
        );
      }).rejects.toThrow();

      const allFilter = filtersListItem[0];
      const allLink = await within(allFilter).findByRole('link');
      userEvent.click(allLink);
    });

    test('should not find the active todo', async () => {
      server.use(
        rest.patch(`${api}/:todoId`, (req, res, ctx) => {
          const { todoId } = req.params;
          return res(
            ctx.status(200),
            ctx.json({
              id: todoId,
              title: 'a title',
              order: 1,
              completed: (req.body as any).completed,
            }),
          );
        }),
      );

      render(
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Todos selectedFilter={FilterType.ALL} />}
            />
            <Route
              path="/active"
              element={<Todos selectedFilter={FilterType.ACTIVE} />}
            />
            <Route
              path="/completed"
              element={<Todos selectedFilter={FilterType.COMPLETED} />}
            />
            <Route path="*" element={<Navigate to="/" />} />
          </Routes>
        </BrowserRouter>,
      );

      const textbox = await screen.findByRole('textbox');
      userEvent.type(textbox, 'a title{enter}');

      const lists = await screen.findAllByRole('list');
      const [todosList, filtersList] = lists;
      const todosListitem = await within(todosList).findAllByRole('listitem');
      expect(todosListitem).toHaveLength(1);

      const filtersListItem = await within(filtersList).findAllByRole(
        'listitem',
      );
      const completedFilter = filtersListItem[2];
      const link = await within(completedFilter).findByRole('link');

      userEvent.click(link);

      await expect(() =>
        within(todosList).findAllByRole('listitem'),
      ).rejects.toThrow();

      const allFilter = filtersListItem[0];
      const allLink = await within(allFilter).findByRole('link');
      userEvent.click(allLink);
    });
  });
});
